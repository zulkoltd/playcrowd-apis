swagger: '2.0'

info:
  title: Playcrowd Hunt Api
  description: The playcrowd hunt API
  version: '@version@'
  contact:
    name: 'Matulo Software Ltd'
    url: 'https://www.matulo.co.uk'
    email: info@matulo.co.uk
  license:
    name: Matulo License
    url: https://www.matulo.co.uk

host: internal.hunt-service

basePath: /

tags:
  - name: Buy In
    description: Players must buy in the play the hunt game
  - name: CashTags
    description: CashTags represent the instructinal content and the prize to be won by the players that complete a hunt
  - name: Hunts
    description: Hunts are games undertaken by players as they purchase clues
  - name: Places
    description: Google places are used to define prize locations.  

produces:
  - application/json

paths:
  /buy-in:
    put:
      tags:
      - Buy In
      summary: Buy in to the hunt game
      description: Players must buy in the play hunt each day
      operationId: buyIn
      produces: 
      - application/json
      parameters:
      - name: useCredits
        in: query
        description: A boolean flag to request that credits should be used to pay for the entry
        required: false
        type: boolean
        default: false
        deprecated: true
      - name: buyInDto
        in: body
        required: false
        schema:
          $ref: '#/definitions/BuyInEditDto'
      responses: 
        200: 
          description: OK Success
        401:
          description: Missing or expired session
        402:
          description: Payment required
        403:
          description: Missing required privileges

  /buy-in/status:
    get:
      tags:
      - Buy In
      summary: Get buy in status for player
      description: Returns the buy in status for the logged in player for the current day
      operationId: getBuyInStatusForPlayer
      produces: 
      - application/json
      responses: 
        200: 
          description: OK Success
          schema:
            $ref: '#/definitions/BuyInDto'
        401:
          description: Missing or expired session
        402: 
          description: Payment required
        403:
          description: Missing required privileges

  /buy-in/status-list:
    get:
      tags:
      - Buy In
      summary: Get buy in status for admin
      description: An admin endpoint for finding all buy in status
      operationId: getBuyInStatusForAdmin
      produces: 
      - application/json
      parameters: 
      - in: query
        name: accountId
        description: The account id to find the buy in status for
        required: false
        type: integer
        format: int64
      - in: query
        name: fromDate
        description: CashTags must open after this date (UTC) to be returned in the results
        required: false
        type: string
        format: 'YYYY-MM-DDThh:mm:ss.fffZ'
      - in: query
        name: toDate
        description: CashTags must open before this date (UTC) to be returned in the results
        required: false
        type: string
        format: 'YYYY-MM-DDThh:mm:ss.fffZ'      
      responses: 
        200: 
          description: OK Success
          schema:
            type: array
            items:
              $ref: '#/definitions/BuyInDto'
        401:
          description: Missing or expired session
        403:
          description: Missing required privileges
        
  /cash-tags:
    get:
      tags:
      - CashTags
      summary: Returns all active CashTags in the system
      description: All CashTags active in the system are returned
      operationId: findCashTags
      produces:
      - application/json
      parameters:
      - in: query
        name: fromDate
        description: CashTags must open after this date (UTC) to be returned in the results
        required: false
        type: string
        format: 'YYYY-MM-DDThh:mm:ss.fffZ'
      - in: query
        name: toDate
        description: CashTags must open before this date (UTC) to be returned in the results
        required: false
        type: string
        format: 'YYYY-MM-DDThh:mm:ss.fffZ'      
      responses:
        200:
          description: OK
          schema:
            type: array
            items:
              $ref: '#/definitions/CashTagDto'
        400:
          description: Invalid format
        401:
          description: Missing or expired session
        402: 
          description: Payment required
        403:
          description: Missing required privileges

    post:
      tags:
      - CashTags
      summary: Create a CashTag
      description: A new CashTag is created in the system
      operationId: createCashTag
      produces:
      - application/json
      parameters:
      - name: cashTagDto
        in: body
        required: true
        schema:
          $ref: '#/definitions/CashTagDto'
      responses:
        201:
          description: CashTag Created
          schema:
            $ref: '#/definitions/CashTagDto'
        400:
          description: Invalid CashTag creation request

  /cash-tags/{id}:
    get:
      tags:
      - CashTags
      summary: Find CashTag by id
      description: The CashTag with the given id will be returned if it exists
      operationId: findCashTag
      produces:
      - application/json
      parameters:
      - name: id
        in: path
        description: The id of the CashTag to find
        required: true
        type: integer
        format: int64
      responses:
        200:
          description: OK
          schema:
            $ref: '#/definitions/CashTagDto'
        400:
          description: Invalid format
        401:
          description: Missing or expired session
        402: 
          description: Payment required
        403:
          description: Missing required privileges
        404:
          description: CashTag Not Found
    put:
      tags:
      - CashTags
      summary: Update an existing CashTag
      description: Update all fields for a given CashTag. 
      operationId: updateCashTag
      produces:
      - application/json
      parameters:
      - name: id
        in: path
        description: The id of the CashTag to update
        required: true
        type: integer
        format: int64
      - name: cashTagDto
        in: body
        required: true
        schema:
          $ref: '#/definitions/CashTagDto'
      responses:
        200:
          description: Success
          schema:
            $ref: '#/definitions/CashTagDto'
        400:
          description: Invalid update request
        404:
          description: CashTag Not Found

  /cash-tags/{id}/start-hunt:
    put:
      tags:
      - CashTags
      - Hunts
      summary: Start hunting
      description: Players must start a hunt to reveal the cash tag hot/cold indicator
      operationId: startHunt
      produces:
      - application/json
      parameters:
      - name: id
        in: path
        description: The id of the CashTag to start hunting for
        required: true
        type: integer
        format: int64
      responses:
        200:
          description: Success
        400:
          description: Invalid start request
        402: 
          description: Payment required
        404:
          description: CashTag Not Found

  /cash-tags/list:
    get:
      tags:
      - CashTags
      summary: Returns all CashTags in the system
      description: All CashTags active in the system are returned
      operationId: findCashTagsList
      produces:
      - application/json
      parameters:
      - in: query
        name: fromDate
        description: CashTags must open after this date (UTC) to be returned in the results
        required: false
        type: string
        format: 'YYYY-MM-DDThh:mm:ss.fffZ'
      - in: query
        name: toDate
        description: CashTags must open before this date (UTC) to be returned in the results
        required: false
        type: string
        format: 'YYYY-MM-DDThh:mm:ss.fffZ'      
      responses:
        200:
          description: OK
          schema:
            type: array
            items:
              $ref: '#/definitions/CashTagDto'
        400:
          description: Invalid format
        401:
          description: Missing or expired session
        402: 
          description: Payment required
        403:
          description: Missing required privileges

  /cash-tags/paginated:
    get:
      tags:
      - CashTags
      summary: Get all CashTags for admin
      description: Returns paginated CashTags for admin users
      operationId: findPaginatedCashTags
      produces:
      - application/json
      parameters:
      - in: query
        name: fromDate
        description: CashTags must open after this date (UTC) to be returned in the results
        required: false
        type: string
        format: 'YYYY-MM-DDThh:mm:ss.fffZ'
      - in: query
        name: toDate
        description: CashTags must open before this date (UTC) to be returned in the results
        required: false
        type: string
        format: 'YYYY-MM-DDThh:mm:ss.fffZ'
      - in: query
        name: cashTagStatus
        description: Filters the list of CashTags by their status. If no status are supplied, then all will be included. If any status are supplied, then only those CashTags will be included.
        required: false
        type: array
        items:
          type: string
          enum: [OPEN, CLOSED]
      - name: page
        in: query
        description: The page of CashTags to return
        required: true
        type: integer
        format: int32
      - name: pageSize
        in: query
        description: The number of CashTags to return per page
        required: true
        type: integer
        format: int32
      responses:
        200:
          description: OK
          schema:
            $ref: '#/definitions/CashTagPageDto'
        400:
          description: Invalid format
        401:
          description: Missing or expired session
        403:
          description: Missing required privileges

  /cash-tags/overview:
    get:
      tags:
      - CashTags
      summary: Get an overview of all cash tags
      description: Returns the number and the value of all open cash tags
      operationId: getOverview
      produces:
      - application/json
      responses:
        200:
          description: OK
          schema:
            $ref: '#/definitions/CashTagOverviewDto'
        400:
          description: Invalid format
        401:
          description: Missing or expired session
        403:
          description: Missing required privileges

  /hunts:
    get:
      tags:
      - Hunts
      summary: Returns all hunts for a given player
      description: All hunts undertaken by the given player
      operationId: findHunts
      produces:
      - application/json
      parameters: 
      - name: accountId
        in: query
        description: The account id of the player to find hunts for
        required: false
        type: integer
        format: int64
      responses:
        200:
          description: OK
          schema:
            type: array
            items:
              $ref: '#/definitions/HuntDto'
        400:
          description: Invalid format
        401:
          description: Missing or expired session
        402: 
          description: Payment required
        403:
          description: Missing required privileges
          
  /hunts/{id}/complete:
    put:
      tags:
      - Hunts
      summary: Complete a hunt
      description: Complete a hunt and win the prize
      operationId: completeHunt
      produces:
      - application/json
      parameters:
      - name: id
        in: path
        description: The id of the hunt completed
        required: true
        type: integer
        format: int64
      responses:
        200:
          description: Success
          schema:
            $ref: '#/definitions/HuntDto'
        400:
          description: Invalid complete request
        402: 
          description: Payment required
        404:
          description: Hunt Not Found

  /hunts/paginated:
    get:
      tags:
      - Hunts
      summary: Get all hunt games for admin
      description: Returns paginated hunts for admin users
      operationId: findPaginatedHunts
      produces:
      - application/json
      parameters:
      - in: query
        name: accountId
        description: An account id used to filter the results. Only hunts for the given player will be returned.
        required: false
        type: integer
        format: int64
      - in: query
        name: clueId
        description: A clue id used to filter the results. Only hunts for the gicen clue will be returned.
        required: false
        type: integer
        format: int64
      - in: query
        name: fromDate
        description: Hunts must have been created after this date (UTC) to be returned in the results
        required: false
        type: string
        format: 'YYYY-MM-DDThh:mm:ss.fffZ'
      - in: query
        name: toDate
        description: Hunts must have been created before this date (UTC) to be returned in the results
        required: false
        type: string
        format: 'YYYY-MM-DDThh:mm:ss.fffZ'
      - in: query
        name: huntStatus
        description: Filters the list of hunts by their hunt status. If no hunt status are supplied, then all will be included. If any hunt status are supplied, then only those huns will be included.
        required: false
        type: array
        items:
          type: string
          enum: [HUNTING, WON, MISSED, VOID]
      - name: page
        in: query
        description: The page of hunts to return
        required: true
        type: integer
        format: int32
      - name: pageSize
        in: query
        description: The number of hunts to return per page
        required: true
        type: integer
        format: int32
      responses:
        200:
          description: OK
          schema:
            $ref: '#/definitions/HuntPageDto'
        400:
          description: Invalid format
        401:
          description: Missing or expired session
        403:
          description: Missing required privileges

  /places:
    get:
      tags:
      - Places
      summary: Get Google places 
      description: Returns a list of possible prize locations based upon the given location and type
      operationId: findPlaces
      produces:
      - application/json
      parameters: 
      - name: location
        in: query
        description: The current location of the map
        required: true
        type: string
      - name: name
        in: query
        description: An optional filter
        required: false
        type: string
      - name: placeType
        in: query
        description: The type of place to search for (cafe, library, restaurant etc)
        required: false
        type: string
      responses:
        200:
          description: OK
          schema:
            type: array
            items:
              $ref: '#/definitions/PlaceDto'
        400:
          description: Invalid format
        401:
          description: Missing or expired session
        403:
          description: Missing required privileges

  /hunt-setting:
    post:
      tags:
        - HuntSettings
      summary: Save hunt setting
      description: Save a global setting of hunt
      operationId: saveHuntSetting
      produces:
        - application/json
      parameters:
        - name: huntSettingDto
          in: body
          required: true
          schema:
            $ref: '#/definitions/HuntSettingDto'
      responses:
        200:
          description: Success
        400:
          description: Invalid format
        401:
          description: Missing or expired session
        403:
          description: Missing required privileges
    get:
      tags:
        - HuntSettings
      summary: Get hunt setting
      description: Get a global setting of hunt
      operationId: getHuntSetting
      produces:
        - application/json
      responses:
        200:
          description: OK
          schema:
            $ref: '#/definitions/HuntSettingDto'
        400:
          description: Invalid format
        401:
          description: Missing or expired session
        403:
          description: Missing required privileges

###########################################################################################
# Definitions
###########################################################################################
definitions:
  CashTagDto:
    type: object
    description: A CashTag represents a prize to be won by players
    properties:
      id:
        type: integer
        format: int64
        description: The id of the CashTag
        default: 0
      openTime:
        type: string
        description: The time that the CashTag will become available
      closeTime:
        type: string
        description: The time that the CashTag will no longer be available
      openDate:
        type: string
        description: The date that the CashTag will become available
      closeDate:
        type: string
        description: The date that the CashTag will no longer be available
      distance:
        type: integer
        description: The distance that the clue is away from the CashTag
      clue:
        type: string
        description: The directional text displayed to the player
      clueLocation:
        type: string
        description: The co-ordinates of the clue
      prizeLocation:
        type: string
        description: The co-ordinates of the CashTag
      prize:
        type: number
        description: The amount of money to be won
      maxWinners:
        type: integer
        description: The number of prizes to be won at this location
      winnerCount:
        type: integer
        description: The number of players that have won the prize
      place:
        $ref: '#/definitions/PlaceDto'
      cashTagStatus:
        $ref: '#/definitions/CashTagStatus'

  CashTagPageDto:
    type: object
    properties:
      totalElements:
        type: integer
        format: int64
        description: The total number of CashTags that can be returned dependent on the page and page size
      totalPages:
        type: integer
        format: int32
        description: The total number of pages that can be returned at the current page size
      nextPage:
        type: integer
        format: int32
        description: The next page of CashTags that can be returned
      hasNext:
        type: boolean
        description: Whether there are more elements to be returned on the next page
      cashTags:
        type: array
        items:
          $ref: "#/definitions/CashTagDto"  

  CashTagOverviewDto:
    type: object
    description: Overview of all open cashtags
    properties:
      numCashTags:
        type: integer
        description: The number of open CashTags
      prizePool:
        type: number
        description: The total amount of money to be won        

  HuntDto:
    type: object
    description: A hunt represents a game played by a player
    properties:
      id:
        type: integer
        format: int64
        description: The id of the hunt
        default: 0
      createdTime:
        type: string
        description: The time that the clue was purchased
      player:
        $ref: '#/definitions/PlayerDto'
      cashTag:
        $ref: '#/definitions/CashTagDto'
      huntStatus:
        $ref: '#/definitions/HuntStatus'

  HuntPageDto:
    type: object
    properties:
      totalElements:
        type: integer
        format: int64
        description: The total number of hunts that can be returned dependent on the page and page size
      totalPages:
        type: integer
        format: int32
        description: The total number of hunts that can be returned at the current page size
      nextPage:
        type: integer
        format: int32
        description: The next page of hunts that can be returned
      hasNext:
        type: boolean
        description: Whether there are more elements to be returned on the next page
      hunts:
        type: array
        items:
          $ref: "#/definitions/HuntDto"

  HuntSettingDto:
    type: object
    description: A hunt setting represents a global setting of game
    properties:
      huntActive:
        type: boolean
        default: false
        description: The active of the hunt
      huntStartTime:
        type: string
        description: The time when next hunt starts
      maxWinPrizeCount:
        type: integer
        description: The maximum number of prizes that can be won by player

  PlayerDto:
    type: object
    description: A player represents the hunting player account
    properties:
      id:
        type: integer
        format: int64
        description: The account id of the player
        default: 0
      username:
        type: string
        description: The players username
      firstName:
        type: string
        description: The players first name
      lastName:
        type: string
        description: The players last name
      avatar:
        type: string
        description: The players chosen avatar

  PlaceDto:
    type: object
    description: A place represents a possible location for a clue
    properties:
      name:
        type: string
        description: The name of the place
      location:
        type: string
        description: The latitude and longitude location of the place
      placeId:
        type: string
        description: The google place id
      placeType:
        type: string
        description: The relevant place type associated with this place type.
      otherPlaceTypes:
        type: array
        description: The available place types associated with this place type.
        items: 
          type: string
      vicinity:
        type: string
        description: The vicinity of the place. Normally the address of the place

  BuyInDto:
    type: object
    description: A buy in dto representing a buy in for a player on a given day
    properties:
      day:
        type: string
        description: The day that the player has bought in to in YYYY-MM-DD format
      buyInStatus:
        $ref: "#/definitions/BuyInStatus"
      buyInMode:
        $ref: "#/definitions/BuyInMode"
      winPrizeCount:
        type: integer
        description: The number of prizes that has been won by player
      player:
        $ref: "#/definitions/PlayerDto"

  BuyInEditDto:
    type: object
    description: A buy in dto representing payment option and mode
    properties:
      paymentOption:
        $ref: "#/definitions/BuyInPaymentOption"
      mode:
        $ref: "#/definitions/BuyInMode"
###########################################################################################
# Enums
###########################################################################################
  CashTagStatus:
    description: The status of a clue. One of OPEN, CLOSED
    type: string
    enum: [OPEN, CLOSED]    
  
  HuntStatus:
    description: The result of a hunt. One of HUNTING, WON, MISSED, VOID
    type: string
    enum: [HUNTING, WON, MISSED, VOID]    

  BuyInStatus:
    description: The status of a buy in request. One of PENDING, COMPLETE, CANCELLED
    type: string
    enum: [PENDING, COMPLETE, CANCELLED]

  BuyInPaymentOption:
    description: The payment option of a hunt. One of CREDITS, CASH
    type: string
    enum: [CREDITS, CASH]

  BuyInMode:
    description: The mode of a hunt. One of GPS, JOYSTICK
    type: string
    enum: [ GPS, JOYSTICK ]