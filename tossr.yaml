swagger: '2.0'

info:
  title: Playcrowd Tossr Api
  description: The playcrowd tossr API
  version: '@version@'
  contact:
    name: 'Matulo Software Ltd'
    url: 'https://www.matulo.co.uk'
    email: info@matulo.co.uk
  license:
    name: Matulo License
    url: https://www.matulo.co.uk

host: internal.tossr-service

basePath: /

tags:
  - name: Overview
    description: Tossr admin statistics provide an overview of the business position
  - name: Spins
    description: Players play the game by spinning a coin. Each spin represents a new game 
  - name: Games
    description: Players play games against other players.
  - name: Rematch
    description: A rematch between the same players
  - name: Jackpots
    description: Tossr jackpots reward players above and beyond the standard tossr game play
  - name: Sequential Jackpot
    description: The sequential jackpot rewards players for matching a predefined sequence of results
  - name: Bad Run Jackpot
    description: The bad run jackpot is an optional jackpot that rewards the player with the longest losing streak in a given time period

produces:
  - application/json

paths:
###########################################################################################
# Overview
###########################################################################################
  /overview:
    get:
      tags:
      - Overview
      summary: Get system overview
      description: Returns a system overview of multiple statistics
      operationId: getOverview
      produces:
      - application/json
      parameters: 
      - in: query
        name: fromDate
        description: Filter the statistics
        required: false
        type: string
        format: 'YYYY-MM-DDThh:mm:ss.fffZ'
      - in: query
        name: toDate
        description: Filter the statistics
        required: false
        type: string
        format: 'YYYY-MM-DDThh:mm:ss.fffZ'      
      responses:
        200:
          description: OK
          schema:
            $ref: '#/definitions/OverviewDto'
        400:
          description: Invalid format
        401:
          description: Missing or expired session
        403:
          description: Missing required privileges
###########################################################################################
# Spins
###########################################################################################
  /spins:
    post:
      tags:
      - Spins
      summary: Spin the coin
      description: Create a new spin and attempt to match with existing spins to create a game
      operationId: spinCoin
      produces:
      - application/json
      parameters:
      - name: spinRequest
        in: body
        required: true
        schema:
          $ref: '#/definitions/SpinRequest'
      responses:
        201:
          description: Spin created
          schema:
            $ref: '#/definitions/SpinDto'
        202:
          description: Spin accepted and game created
          schema:
            $ref: '#/definitions/SpinDto'
        400:
          description: Invalid or missing parameters
        401:
          description: Missing or expired session
        403:
          description: Missing required privileges
          
  /spins/{spinId}:
    get:
      tags:
      - Spins
      summary: Get spin status 
      description: Returns the status of the requested spin
      operationId: getStatus
      produces:
      - application/json
      parameters:
      - name: spinId
        in: path
        description: The id of the spin
        required: true
        type: integer
        format: int64     
      responses:
        200:
          description: OK
          schema:
            $ref: '#/definitions/SpinDto'
        400:
          description: Invalid or missing parameters
        401:
          description: Missing or expired session
        403:
          description: Missing required privileges
        404:
          description: Not Found         

###########################################################################################
# Games
###########################################################################################
  /games:
    get:
      tags:
      - Games
      summary: Find games
      description: Returns a paginated list of games based upon the supplied filters
      operationId: findGames
      produces:
      - application/json
      parameters:
      - name: page
        in: query
        description: The page of games to return
        required: true
        type: integer
        format: int32
      - name: accountId
        in: query
        description: An account id to filter the games by
        required: false
        type: integer
        format: int64     
      - in: query
        name: fromDate
        description: Games must have been created after this date (UTC) to be returned in the results
        required: false
        type: string
        format: 'YYYY-MM-DDThh:mm:ss.fffZ'
      - in: query
        name: toDate
        description: Games must have been created before this date (UTC) to be returned in the results
        required: false
        type: string
        format: 'YYYY-MM-DDThh:mm:ss.fffZ'
      responses:
        200:
          description: OK
          schema:
            $ref: '#/definitions/GamePageDto'
        400:
          description: Invalid or missing parameters
        401:
          description: Missing or expired session
        403:
          description: Missing required privileges

  /games/{gameId}/rematch:
    get:
      tags:
      - Rematch
      summary: Get rematch information 
      description: Returns information about any impending rematch
      operationId: getRematchInfo
      produces:
      - application/json
      parameters:
      - name: gameId
        in: path
        description: The id of the game to find rematch information for
        required: true
        type: integer
        format: int64     
      responses:
        200:
          description: OK
          schema:
            $ref: '#/definitions/RematchDto'
        400:
          description: Invalid or missing parameters
        401:
          description: Missing or expired session
        403:
          description: Missing required privileges
        404:
          description: Not Found
    put:
      tags:
      - Rematch
      summary: Update the players rematch preference
      description: Request, accept or decline a rematch
      operationId: rematch
      produces:
      - application/json
      parameters:
      - name: gameId
        in: path
        description: The id of the game to update rematch preference for
        required: true
        type: integer
        format: int64     
      - in: query
        name: rematchPreference
        required: true
        type: boolean
        description: The users preference towards a rematch
      responses:
        200:
          description: OK
          schema:
            $ref: '#/definitions/RematchDto'
        400:
          description: Invalid or missing parameters
        401:
          description: Missing or expired session
        403:
          description: Missing required privileges
        404:
          description: Not Found

###########################################################################################
# Jackpots
###########################################################################################
  /jackpots/{jackpotType}:
    put:
      tags:
      - Jackpots
      summary: Update a jackpot status
      description: Update the status of a jackpot
      operationId: updateJackpotStatus
      produces:
      - application/json
      parameters:
      - in: path
        name: jackpotType
        required: true
        type: string
      - in: query
        name: active
        required: true
        type: boolean
        default: true
      responses:
        200:
          description: Jackpot status updated
        400:
          description: Invalid or missing parameters
        401:
          description: Missing or expired session
        403:
          description: Missing required privileges

  /jackpots/progress:
    get:
      tags:
      - Jackpots
      summary: Get jackpot progress 
      description: Returns the progress of all jackpots for the current user 
      operationId: getProgress
      produces:
      - application/json
      responses:
        200:
          description: OK
          schema:
             $ref: '#/definitions/JackpotProgressDto'
        400:
          description: Invalid or missing parameters
        401:
          description: Missing or expired session
        403:
          description: Missing required privileges

###########################################################################################
# Sequential Jackpot
###########################################################################################
  /sequential-jackpots:
    get:
      tags:
      - Sequential Jackpot
      summary: Get sequential jackpots 
      description: Returns a list of sequential jackpots
      operationId: getSequentialJackpots
      produces:
      - application/json
      parameters:
      - name: jackpotStatus
        in: query
        description: The requested status of the seqential jackpots to return
        type: string     
      responses:
        200:
          description: OK
          schema:
            type: array
            items:
              $ref: '#/definitions/SequentialJackpotDto'
        400:
          description: Invalid or missing parameters
        401:
          description: Missing or expired session
        403:
          description: Missing required privileges
    post:
      tags:
      - Sequential Jackpot
      summary: Create a sequential jackpot
      description: Create a new sequential jackpot. This will create a pending jackpot if there is a already a jackpot in play.
      operationId: createSequentialJackpot
      produces:
      - application/json
      parameters:
      - name: jackpotRequest
        in: body
        required: true
        schema:
          $ref: '#/definitions/JackpotRequest'
      responses:
        201:
          description: Sequential Jackpot created
          schema:
            $ref: '#/definitions/SequentialJackpotDto'
        400:
          description: Invalid or missing parameters
        401:
          description: Missing or expired session
        403:
          description: Missing required privileges

  /sequential-jackpots/{id}:
    put:
      tags:
      - Sequential Jackpot
      summary: Update a sequential jackpot
      description: Update a pending sequential jackpot.
      operationId: updateSequentialJackpot
      produces:
      - application/json
      parameters:
      - name: id
        in: path
        description: The id of the sequential jackpot to update
        required: true
        type: integer
        format: int64     
      - name: jackpotRequest
        in: body
        required: true
        schema:
          $ref: '#/definitions/JackpotRequest'
      responses:
        200:
          description: Sequential Jackpot updated
          schema:
            $ref: '#/definitions/SequentialJackpotDto'
        400:
          description: Invalid or missing parameters
        401:
          description: Missing or expired session
        403:
          description: Missing required privileges

###########################################################################################
# Bad Run Jackpot
###########################################################################################
  /bad-run-jackpots:
    get:
      tags:
      - Bad Run Jackpot
      summary: Get bad run jackpots 
      description: Returns a list of bad run jackpots
      operationId: getBadRunJackpots
      produces:
      - application/json
      parameters:
      - name: jackpotStatus
        in: query
        description: The requested status of the bad run jackpots to return
        type: string     
      responses:
        200:
          description: OK
          schema:
            type: array
            items:
              $ref: '#/definitions/BadRunJackpotDto'
        400:
          description: Invalid or missing parameters
        401:
          description: Missing or expired session
        403:
          description: Missing required privileges
    post:
      tags:
      - Bad Run Jackpot
      summary: Create a bad run jackpot
      description: Create a new bad run jackpot. This will create a pending jackpot if there is a already a jackpot in play.
      operationId: createBadRunJackpot
      produces:
      - application/json
      parameters:
      - name: jackpotRequest
        in: body
        required: true
        schema:
          $ref: '#/definitions/JackpotRequest'
      responses:
        201:
          description: Bad Run Jackpot created
          schema:
            $ref: '#/definitions/BadRunJackpotDto'
        400:
          description: Invalid or missing parameters
        401:
          description: Missing or expired session
        403:
          description: Missing required privileges

  /bad-run-jackpots/{id}:
    put:
      tags:
      - Bad Run Jackpot
      summary: Update a bad run jackpot
      description: Update a pending bad run jackpot.
      operationId: updateBadRunJackpot
      produces:
      - application/json
      parameters:
      - name: id
        in: path
        description: The id of the bad run jackpot to update
        required: true
        type: integer
        format: int64     
      - name: jackpotRequest
        in: body
        required: true
        schema:
          $ref: '#/definitions/JackpotRequest'
      responses:
        200:
          description: Bad Run Jackpot updated
          schema:
            $ref: '#/definitions/BadRunJackpotDto'
        400:
          description: Invalid or missing parameters
        401:
          description: Missing or expired session
        403:
          description: Missing required privileges

  /bad-run-jackpots/{id}/opt-in:
    post:
      tags:
      - Bad Run Jackpot
      summary: Opt in to bad run jackpot
      description: Opt in to the specified bad run jackpot.
      operationId: optIn
      produces:
      - application/json
      parameters:
      - name: id
        in: path
        description: The id of the bad run jackpot to opt in to
        required: true
        type: integer
        format: int64     
      responses:
        200:
          description: Opt in success
        400:
          description: Invalid or missing parameters
        401:
          description: Missing or expired session
        403:
          description: Missing required privileges

  /bad-run-jackpots/{id}/opt-out:
    post:
      tags:
      - Bad Run Jackpot
      summary: Opt out of bad run jackpot
      description: Opt out of the specified bad run jackpot.
      operationId: optOut
      produces:
      - application/json
      parameters:
      - name: id
        in: path
        description: The id of the bad run jackpot to opt out of
        required: true
        type: integer
        format: int64     
      responses:
        200:
          description: Opt out success
        400:
          description: Invalid or missing parameters
        401:
          description: Missing or expired session
        403:
          description: Missing required privileges

###########################################################################################
# Definitions
###########################################################################################
definitions:
  OverviewDto:
    type: object
    description: An overview of the tossr system
    properties:
      totals:
        $ref: '#/definitions/StatisticsDto'  
      filtered: 
        $ref: '#/definitions/StatisticsDto'

  StatisticsDto:
    type: object
    description: An overview of the tossr system
    properties:
      games: 
        type: integer
        description: The number of games
      stakes: 
        type: number
        description: The sum af all stakes
      winnings: 
        type: number
        description: The sum of all winnings
      botGames: 
        type: integer
        description: The number of bot games
      botWinnings: 
        type: number
        description: The sum of all bot winnings
      rake: 
        type: number
        description: The sum of all rake
      badRunJackpotBuyIns: 
        type: integer
        description: The number of bad run jackpot buy ins
      badRunJackpotPayouts: 
        type: number
        description: The sum of all bad run jackpots
      sequentialJackpotPayouts: 
        type: number
        description: The sum of all sequential jackpots
      jackpotPayouts: 
        type: number
        description: The sum of all jackpot payouts


  SpinRequest:
    type: object
    properties:
      accountId:
        type: integer
        format: int64
        description: The id of the player spinning
      stake:
        type: number
        description: The amount staked by the player
      coinSelection:
        $ref: '#/definitions/CoinSelection'

  SpinDto:
    type: object
    properties:
      id:
        type: integer
        format: int64
        description: The id of the spin
      stake:
        type: number
        description: The amount staked by the player
      winnings:
        type: number
        description: The amount won
      player:
        $ref: '#/definitions/PlayerDto'
      coinSelection:
        $ref: '#/definitions/CoinSelection'
      game:
        $ref: '#/definitions/GameDto'
      spinStatus:
        $ref: '#/definitions/SpinStatus'

  RematchDto:
    type: object
    properties:
      rematchSpinId:
        type: integer
        format: int64
        description: The id of the spin in the rematch game
      rematchStatus:
        $ref: '#/definitions/RematchStatus'
      rematchPreference:
        $ref: '#/definitions/RematchPreference'
      opponentRematchPreference:
        $ref: '#/definitions/RematchPreference'
  
  GameDto:
    type: object
    properties:
      id:
        type: integer
        format: int64
        description: The id of the game
      rematch:
        type: boolean
        description: A flag to state that this game is a rematch
      winningCoinSelection:
        $ref: '#/definitions/CoinSelection'
      spins:
        type: array
        items:
          $ref: '#/definitions/SpinDto'
      createdTime:
        type: string
        description: The time that the game was started

  GamePageDto:
    type: object
    properties:
      totalElements:
        type: integer
        format: int64
        description: The total number of items that can be returned dependent on the page and page size
      totalPages:
        type: integer
        format: int32
        description: The total number of pages that can be returned at the current page size
      nextPage:
        type: integer
        format: int32
        description: The next page of items that can be returned
      hasNext:
        type: boolean
        description: Whether there are more elements to be returned on the next page
      games:
        type: array
        items:
          $ref: "#/definitions/GameDto"     

  JackpotRequest:
    type: object
    properties:
      minimumPayout:
        type: number
        description: The amount of money in the jackpot pool
      rake:
        type: number
        description: The rake percentage earned on the jackpot
      jackpotFrequency:
        $ref: '#/definitions/JackpotFrequency'
      expirationTime:
        type: string
        description: The time that the jackpot will expire in 24 hour format e.g. 11:00 or 23:00

  SequentialJackpotDto:
    type: object
    properties:
      id:
        type: integer
        format: int64
        description: The id of the jackpot
      jackpotSequence:
        type: string
        description: The generated jackpot sequence.
      jackpotStatus:
        $ref: '#/definitions/JackpotStatus'
      startTime:
        type: string
        description: The time that the jackpot was started
      jackpotFrequency:
        $ref: '#/definitions/JackpotFrequency'
      expirationTime:
        type: string
        description: The time that the jackpot will expire in 24 hour format e.g. 11:00 or 23:00
      expiryTime:
        type: string
        description: The time that the jackpot will expire
      minimumPayout:
        type: number
        description: The amount of money in the jackpot pool
      rake:
        type: number
        description: The rake percentage earned on the jackpot

  BadRunJackpotDto:
    type: object
    properties:
      id:
        type: integer
        format: int64
        description: The id of the jackpot
      jackpotStatus:
        $ref: '#/definitions/JackpotStatus'
      startTime:
        type: string
        description: The time that the jackpot was started
      jackpotFrequency:
        $ref: '#/definitions/JackpotFrequency'
      expirationTime:
        type: string
        description: The time that the jackpot will expire in 24 hour format e.g. 11:00 or 23:00
      expiryTime:
        type: string
        description: The time that the jackpot will expire
      minimumPayout:
        type: number
        description: The amount of money in the jackpot pool
      rake:
        type: number
        description: The rake percentage earned on the jackpot

  JackpotProgressDto:
    type: object
    properties:
      sequentialJackpotProgress:
        $ref: '#/definitions/SequentialJackpotProgressDto'
      badRunJackpotProgress:
        $ref: '#/definitions/BadRunJackpotProgressDto'

  SequentialJackpotProgressDto:
    type: object
    properties:
      sequentialJackpotId:
        type: integer
        format: int64
        description: The id of the jackpot
      pool:
        type: number
        description: The amount of money currently in the jackpot pool
      jackpotSequence:
        type: string
        description: The generated jackpot sequence.
      playerSequence:
        type: string
        description: The jackpot sequence for the player     
      
  BadRunJackpotProgressDto:
    type: object
    properties:
      badRunJackpotId:
        type: integer
        format: int64
        description: The id of the jackpot
      optedIn:
        type: boolean
        description: The opt in status of the player
      pool:
        type: number
        description: The amount of money currently in the jackpot pool
      badRunCount:
        type: integer
        description: The length of the players current losing streak
      worstRunCount:
        type: integer
        description: The length of the players worst losing streak
      expiryTime:
        type: string
        description: The time that the jackpot will expire
      
  PlayerDto:
    type: object
    description: A player taking part in a game
    properties:
      id:
        type: integer
        format: int64
        description: The id of the player
        default: 0
      username:
        type: string
        description: The players username
      location:
        type: string
        description: The players location
      bot:
        type: boolean
        description: A flag to denote that this player is a bot player

###########################################################################################
# Enums
###########################################################################################
  CoinSelection:
    description: The players selection, one of HEADS, TAILS
    type: string
    enum: [HEADS, TAILS]    
    
  SpinStatus:
    description: The result of the game, one of PENDING, SPINNING, WON, LOST, VOID
    type: string
    enum: [PENDING, SPINNING, WON, LOST, VOID]            

  RematchStatus:
    description: The rematch status. One of OPEN, EXPIRED
    type: string
    enum: [OPEN, EXPIRED]            

  RematchPreference:
    description: The rematch preference regarding a rematch. One of NONE, WANTED, NOT_WANTED
    type: string
    enum: [NONE, WANTED, NOT_WANTED]            

  JackpotType: 
    description: The type of jackpot mechanism. One of SEQUENTIAL, BAD_RUN
    type: string
    enum: [SEQUENTIAL, BAD_RUN]
     
  JackpotStatus: 
    description: The status of a jackpot
    type: string
    enum: [PENDING, OPEN, SETTLED]
  
  JackpotFrequency:
    description: The frequency that a jackpot should execute
    type: string
    enum: [HOULY, DAILY, WEEKLY, FORTNIGHTLY, MONTHLY]