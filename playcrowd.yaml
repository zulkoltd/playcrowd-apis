swagger: '2.0'

info:
  title: Playcrowd Api
  description: The playcrowd API
  version: '@version@'
  contact:
    name: 'Matulo Software Ltd'
    url: 'https://www.matulo.co.uk'
    email: info@matulo.co.uk
  license:
    name: Matulo License
    url: https://www.matulo.co.uk

host: internal.playcrowd-service

basePath: /

tags:
  - name: Overview
    description: PlayCrowd admin statistics provide an overview of the business position

produces:
  - application/json

paths:

###########################################################################################
# Overview
###########################################################################################
  /overview:
    get:
      tags:
      - Overview
      summary: Get system overview
      description: Returns a system overview of multiple statistics
      operationId: getOverview
      produces:
      - application/json
      parameters: 
      - in: query
        name: fromDate
        description: CashTags must open after this date (UTC) to be returned in the results
        required: false
        type: string
        format: 'YYYY-MM-DDThh:mm:ss.fffZ'
      - in: query
        name: toDate
        description: CashTags must open before this date (UTC) to be returned in the results
        required: false
        type: string
        format: 'YYYY-MM-DDThh:mm:ss.fffZ'      
      responses:
        200:
          description: OK
          schema:
            $ref: '#/definitions/OverviewDto'
        400:
          description: Invalid format
        401:
          description: Missing or expired session
        403:
          description: Missing required privileges

###########################################################################################
# PromoCodes
###########################################################################################
  /promo-codes:
    get:
      tags:
      - Promo Codes
      summary: Find promo codes
      description: Returns all promo codes
      operationId: findPromoCodes
      produces:
      - application/json
      parameters:
      - name: page
        in: query
        description: The page of items to return
        required: true
        type: integer
        format: int32
      - name: filter      
        in: query
        description: A filter to apply to the search. Either ALL or ACTIVE
        required: false
        type: string
      responses:
        200:
          description: OK
          schema:
            $ref: "#/definitions/PromoCodePageDto"
        400:
          description: Invalid format
        401:
          description: Missing or expired session
        403:
          description: Missing required privileges

    post:
      tags:
      - Promo Codes
      summary: Create a new promo code
      description: Create a new promo code
      operationId: createPromoCode
      produces:
      - application/json
      parameters:
      - name: promoCodeDto
        in: body
        required: true
        schema:
          $ref: '#/definitions/PromoCodeDto'
      responses:
        201:
          description: Code Created
          schema:
            $ref: '#/definitions/PromoCodeDto'
        400:
          description: Invalid creation request

  /promo-codes/{id}:
    put:
      tags:
      - Promo Codes
      summary: Update a promo code
      description: Update an existing promo code
      operationId: updatePromoCode
      produces:
      - application/json
      parameters:
      - name: id
        in: path
        description: The id of the promo code to update
        required: true
        type: integer
        format: int64
      - name: promoCodeDto
        in: body
        required: true
        schema:
          $ref: '#/definitions/PromoCodeDto'
      responses:
        200:
          description: Code Updated
          schema:
            $ref: '#/definitions/PromoCodeDto'
        400:
          description: Invalid update request

###########################################################################################
# Definitions
###########################################################################################
definitions:
  
  PromoCodePageDto:
    type: object
    properties:
      totalItems:
        type: integer
        format: int64
        description: The total number of items that can be returned dependent on the page and page size
      totalPages:
        type: integer
        format: int32
        description: The total number of items that can be returned at the current page size
      nextPage:
        type: integer
        format: int32
        description: The next page that can be returned
      hasNext:
        type: boolean
        description: Whether there are more elements to be returned on the next page
      items:
        type: array
        items:
          $ref: "#/definitions/PromoCodeDto"  

  OverviewDto:
    type: object
    description: An overview of the playcrowd system
    properties:
      totalSignUps:
        type: integer
        description: The total number of sign ups
      liability:
        type: number
        description: How much money is in players wallet 
      signUps:
        type: integer
        description: The number of sign ups
      depositors:
        type: integer
        description: Total depositing users 
      stakes: 
        $ref: '#/definitions/StakesDto'
      winnings: 
        $ref: '#/definitions/WinningsDto'
      rake:
        $ref: '#/definitions/RakesDto'
      netRake:
        type: number
        description: Rake generated net of jackpot rake backs
      rakeJackpotContributions:
        type: number
        description: The total amount of rake given towards jackpots
      cashJackpotContributions:
        type: number
        description: The total amount of cash given towards jackpots
      jackpotPayouts:
        type: number
        description: Total paid out in jackpots (i.e. tossr jackpots, cashtags, chop tournaments)
      botBank:
        type: number
        description: The amount of cash the bot wins/loses on top of rake (tossr only)
      deposits:
        type: number
        description: Total amount deposited by players
      withdrawals:
        type: number
        description: Total amount withdrawn by players
      selfExclusions:
        type: integer
        description: Running total of self exlusions
      bettingLimits:
        type: integer
        description: Running total of how many times betting limits we set
      depositLimits:
        type: integer
        description: Running total of how many times deposit limits we set

  StakesDto:
    type: object
    description: A composite object containing all stakes per game
    properties:
      total:
        type: number
        description: The total stakes
      tossr:
        type: number
        description: The tossr stakes
      hunt:
        type: number
        description: The hunt buy ins
      chop:
        type: number
        description: The chop buy ins

  WinningsDto:
    type: object
    description: A composite object containing all winnings per game
    properties:
      total:
        type: number
        description: The total winnings
      tossr:
        type: number
        description: The tossr winnings
      hunt:
        type: number
        description: The hunt winnings
      chop:
        type: number
        description: The chop winnings
        
  RakesDto:
    type: object
    description: A composite object containing all rakes per game
    properties:
      total:
        type: number
        description: The total amount of rake generated
      tossr:
        type: number
        description: The tossr rake from coin tosses and rake on opt-in jackpot
      hunt:
        type: number
        description: The rake from daily entry fee
      chop:
        type: number
        description: The rake from tournament entry fees

  PromoCodeDto:
    type: object
    description: A promo code
    properties:
      id:
        type: integer
        format: int64
      credits:
        type: integer
        format: int32
        description: The number of credits applied to the promo code
      active:
        type: boolean
        description: A flag to denote that the code active or not 
      code:
        type: string
        description: The promo code value
        